#!/bin/bash

if [ -d "build" ]; then
	rm -r "build"
fi

mkdir build
cd build
ln -s ../config config
cmake ../
make
echo ----------------------------------------------------------------------------
./ConcuPark -d
echo ----------------------------------------------------------------------------
cd ..	# sale de build/
