# ConcuPark

#### Primer Trabajo Práctico de Técnicas de Programación Concurrente I

### Grupo

| Nombre            | Padrón | Mail                     |
|:-----------------:|:------:|:------------------------:|
|Andrés Predazzi    | 93.057 | apredazzi@gmail.com      |
|Santiago Fernández | 94.489 | fernandezsantid@gmail.com|
|Tobías Lichtig     | 94.981 | toblich@gmail.com        |


### Uso

Con el script `rebuild.sh` se compila y ejecuta el programa. Si no, una vez compilado, se ejecuta de la siguiente manera:

` ConcuPark [-d] [-l logName] [-p personasFile] [-j juegosFile] [-s sleepTime] [-a alarmTime]`

donde 

- `-d` habilita el modo debug 

- `-l` especifica el nombre del archivo de log que se creará (sobreescribiendo el existente, si lo hubiera). Por default se nombra con la fecha y hora, con extensión `.log`.

- `-p` especifica el archivo de configuración de las personas [default: config/personas].

- `-j` especifica el archivo de configuración de las juegos [default: config/juegos].

- `-s` especifica la duración de los juegos y el período con el Administrador controla la caja [default: 5].

- `-a` especifica cuánto tiempo pueden llegar a dedicar como máximo los juegos a sacar gente de su fila [default: 2].



### Configuración

- En el archivo de configuración de las personas [default: config/personas], poner un número entero por cada persona indicando su presupuesto al momento de entrar al parque.

- En el archivo de configuración de los juegos [default: config/juegos], poner dos números enteros por cada juego indicando su capacidad y costo, respectivamente.