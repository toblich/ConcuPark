#include "Logger.h"

using namespace std;
using std::chrono::steady_clock;

ofstream Logger::logFile = ofstream();

bool Logger::modoDebug = false;

steady_clock::time_point Logger::initTime = steady_clock::now();

int Logger::lockfd = -1;

long Logger::getTimestamp () {
    steady_clock::time_point now = steady_clock::now();
    return ((now - initTime) * steady_clock::period::num * 1000000 /
            steady_clock::period::den).count(); // En microsegundos
}

void Logger::iniciar (bool modoDebug, string logName) {
    Logger::modoDebug = modoDebug;

    if (not Logger::modoDebug) {
        return;
    }

    time_t rawTime = time(nullptr);
    string timestamp = ctime(&rawTime);
    timestamp.pop_back(); // Le saca en '\n' final

    if (logName.empty()) {
        logName = timestamp + LOG_EXT;
    }

    logFile.open(logName);
    lockfd = open(logName.c_str(), O_CREAT | O_WRONLY);

    if (lockfd == -1) {
        cerr << "Error al abrir el archivo para lock: " << logName << endl;
        logFile.close();
        exit(-1);
    }

    log("INICIO DEL LOG, " + timestamp);
}

void Logger::finalizar () {
    if (not modoDebug)
        return;

    log("FIN DEL LOG (pid " + to_string(getpid()) + ")");
    logFile.close();
    close(lockfd);
}

void Logger::log (const string& mensaje) {
    if (not modoDebug)
        return;

    flock lock;
    lock.l_type = F_WRLCK;
    lock.l_whence = SEEK_SET;
    lock.l_start = 0;
    lock.l_len = 0;
    fcntl(lockfd, F_SETLKW, &lock);

    std::cout << mensaje << std::endl;
    logFile << getTimestamp() << " microsegundos, PID: " << getpid() << "    " << mensaje << endl;

    lock.l_type = F_UNLCK;
    fcntl(lockfd, F_SETLK, &lock);
}







