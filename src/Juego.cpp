#include "Juego.h"
#include "Parque.h"

using std::to_string;

Juego::Juego (int capacidad, int costo, unsigned int sleepTime, unsigned int alarmTime) :
        genteAdentro(), sigAlarm(SIGALRM) {
    this->capacidad = capacidad;
    this->costo = costo;
    this->sleepTime = sleepTime;
    this->alarmTime = alarmTime;
}

Juego::Juego (const Juego& juego) : Proceso(juego), genteAdentro(juego.genteAdentro), sigAlarm(juego.sigAlarm) {
    this->capacidad = juego.capacidad;
    this->costo = juego.costo;
}

Juego::~Juego () {
}

void Juego::comenzar () {
    Logger::log("Comienza el proceso del Juego");
    std::cout << "Juego: " << getpid() << std::endl;

    SignalEventHandler sigInt(SIGINT);
    SignalHandler::getInstance()->registrarHandler(SIGALRM, &sigAlarm);
    SignalHandler::getInstance()->registrarHandler(SIGINT, &sigInt);

    while (not sigInt.getSonoAlarma()) {
        sacarPersonasDeLaFila();
        if (genteAdentro.size() > 0) {
            arrancarJuego();
            bajarPersonas();
        }
    }
    Logger::log("Cerrando todo");
    SignalHandler::getInstance()->removerHandler(SIGALRM);
    SignalHandler::getInstance()->removerHandler(SIGINT);
}


void Juego::agregarPersonaAFila (Persona& persona) {
    persona.pagar(costo);
    Parque::getInstance()->cobrar(costo);
    int id = persona.getId();
    pipe.escribir(&id, sizeof(id));
}

void Juego::sacarPersonasDeLaFila () {
    bool hayAlarmaAnterior = (alarm(alarmTime) != 0);
    sigAlarm.reiniciar(hayAlarmaAnterior);

    Logger::log("Empezando a sacar gente de la fila");
    while (not sigAlarm.getSonoAlarma() and (genteAdentro.size() < capacidad)) {
        int id = -1;
        Logger::log("Voy a sacar una persona de la fila");
        pipe.leer(&id, sizeof(id));

        if (id != -1) {
            Logger::log("Saque de la fila a la persona " + to_string(id));
            Persona* persona = Parque::getInstance()->getPersona(id);
            persona->subirse();
            genteAdentro.push_back(persona);
        }
    }
    Logger::log("Termine de sacar gente de la fila");
}

void Juego::arrancarJuego () {
    sigAlarm.bloquearSenial();
    Logger::log("Juego: Arranco");
    sleep(sleepTime);
    Logger::log("Juego: Termino");
    sigAlarm.desbloquearSenial();
}

void Juego::bajarPersonas () {
    Logger::log("Juego: Bajo personas");
    for (auto& persona : genteAdentro) {
        persona->bajarse();
    }
    genteAdentro.clear();

}





















