#ifndef CONCUPARK_LOGGER_H
#define CONCUPARK_LOGGER_H

#include <chrono>
#include <ctime>
#include <string>
#include <fstream>
#include <unistd.h>
#include <fcntl.h>
#include <iostream>

#define LOG_EXT ".log"

class Logger {

public:

    static void iniciar (bool modoDebug, std::string logName);

    static void finalizar ();

    static void log (const std::string& mensaje);

private:

    static std::ofstream logFile;

    static int lockfd;

    static std::chrono::steady_clock::time_point initTime;

    static long getTimestamp ();

    static bool modoDebug;
};


#endif //CONCUPARK_LOGGER_H
