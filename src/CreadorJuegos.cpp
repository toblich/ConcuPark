#include "CreadorJuegos.h"

using namespace std;

CreadorJuegos::CreadorJuegos (const Opciones& opciones) : juegos() {
    parsearJuegos(opciones);
}

CreadorJuegos::~CreadorJuegos () {
    for (auto& juego : juegos) {
        delete juego;
    }
}

void CreadorJuegos::parsearJuegos (const Opciones& opciones) {
    ifstream file(opciones.juegos);
    int capacidad, costo;
    while (file >> capacidad >> costo) {
        juegos.push_back(new Juego(capacidad, costo, opciones.sleep, opciones.alarm));
    }
    file.close();
}


