#ifndef CONCUPARK_OPCIONES_H
#define CONCUPARK_OPCIONES_H

#include <string>

using std::string;

typedef struct {
    bool debug = false;
    string personas = "config/personas";
    string juegos = "config/juegos";
    string logName = "";
    unsigned int sleep = 5;
    unsigned int alarm = 2;
} Opciones;

#endif //CONCUPARK_OPCIONES_H
