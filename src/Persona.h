#ifndef CONCUPARK_PERSONA_H
#define CONCUPARK_PERSONA_H

#include <vector>
#include <utility>
#include "Proceso.h"
#include "Logger.h"

class Juego;

#include "Juego.h"

#define SUBIR 0
#define BAJAR 1

class Persona : public Proceso {
public:
    Persona (int dineroInicial, std::vector<Juego*>& juegos, int id);

    Persona (const Persona& p);

    virtual ~Persona ();

    void pagar (int costo);

    int getId () const { return id; }

    void bajarse ();

    void subirse ();

private:
    virtual void comenzar ();

    void jugar ();

    const int getCostoMinimo ();

    int dinero;
    int id;
    std::vector<Juego*>& juegosExistentes;
};


#endif //CONCUPARK_PERSONA_H
