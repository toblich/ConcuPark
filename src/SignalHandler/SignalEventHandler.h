#ifndef SIGINT_HANDLER_H_
#define SIGINT_HANDLER_H_

#include <signal.h>
#include <assert.h>

#include "EventHandler.h"

class SignalEventHandler : public EventHandler {

private:
    sig_atomic_t sonoAlarma;
    bool hayAlarmaAnterior;
    int signum;

    std::string nombreSenial (int signum) {
        switch (signum) {
            case SIGINT: return "SIGINT";
            case SIGALRM: return "SIGALRM";
            case SIGPIPE: return "SIGPIPE";
            case SIGQUIT: return "SIGQUIT";
            default: return std::to_string(signum) + " (desconocida)";
        }
    }

public:

    SignalEventHandler (int signum) : sonoAlarma(0), hayAlarmaAnterior(false), signum(signum) {
    }

    ~SignalEventHandler () {
    }

    virtual int handleSignal (int signum) {
        assert (signum == this->signum);
        Logger::log("Se recibio la senial " + nombreSenial(signum));
        if (hayAlarmaAnterior) {
            hayAlarmaAnterior = false;
        } else {
            this->sonoAlarma = 1;
        }
        Logger::log("Se proceso la senial " + nombreSenial(signum));
        return 0;
    }

    sig_atomic_t getSonoAlarma () const {
        return this->sonoAlarma;
    }

    void reiniciar (bool hayAlarmaAnterior) {
        this->sonoAlarma = 0;
        this->hayAlarmaAnterior = hayAlarmaAnterior;
    }

    void bloquearSenial () {
        sigset_t sa;
        sigemptyset(&sa);
        sigaddset(&sa, signum);
        sigprocmask(SIG_BLOCK, &sa, NULL);
    }

    void desbloquearSenial () {
        sigset_t sa;
        sigemptyset(&sa);
        sigaddset(&sa, signum);
        sigprocmask(SIG_UNBLOCK, &sa, NULL);
    }
};

#endif /* SIGINT_HANDLER_H_ */
