#ifndef CONCUPARK_CREADORPERSONAS_H
#define CONCUPARK_CREADORPERSONAS_H

#include <fstream>
#include "Persona.h"

class CreadorPersonas {
public:
    CreadorPersonas (const std::string& filename, std::vector<Juego*>& juegos);

    virtual ~CreadorPersonas ();

    const std::vector<Persona*>& getPersonas () const {
        return personas;
    }

private:
    std::vector<Persona*> personas;

    void parsearPersonas (const std::string& filename, std::vector<Juego*>& juegos);
};


#endif //CONCUPARK_CREADORPERSONAS_H
