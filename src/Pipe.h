#ifndef CONCUPARK_PIPE_H
#define CONCUPARK_PIPE_H

#include <unistd.h>
#include "Logger.h"

#define READ 0
#define WRITE 1

typedef enum {
    ESCRITURA, LECTURA, INDEFINIDO
} MODO_PIPE;

class Pipe {
public:
    Pipe ();

    Pipe (const Pipe& pipe);

    virtual ~Pipe ();

    void setModo (MODO_PIPE nuevoModo);

    void escribir (const void* mensaje, size_t tamanio);

    void leer (void* mensaje, size_t tamanio);

private:
    MODO_PIPE modo;
    int pipefd[2];
};


#endif //CONCUPARK_PIPE_H
