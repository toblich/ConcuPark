#include <iostream>
#include <wait.h>
#include "Persona.h"
#include "Parque.h"
#include "CreadorJuegos.h"
#include "CreadorPersonas.h"
#include "Administrador.h"

using namespace std;

Opciones parsearParametros (int argc, char** argv);

void lanzarParque (const pid_t mainPid, Parque* parque, pid_t adminPid);

void lanzarJuegos (const pid_t mainPid, const vector<Juego*>& juegos);

void lanzarPersonas (const pid_t mainPid, const vector<Persona*>& personas);

void waitHijos (const pid_t mainPid, const vector<Juego*>& juegos, const vector<Persona*>& personas);


int main (int argc, char** argv) {
    const pid_t mainPid = getpid();
    cout << "Main: " << mainPid << endl;

    Opciones opciones = parsearParametros(argc, argv);
    Logger::iniciar(opciones.debug, opciones.logName);

    Parque* parque = Parque::getInstance();
    Administrador administrador(parque->getCaja(), parque->getSemaforoCaja(), opciones.sleep);

    CreadorJuegos creadorJuegos(opciones);
    vector<Juego*> juegos = creadorJuegos.getJuegos();

    CreadorPersonas creadorPersonas(opciones.personas, juegos);
    vector<Persona*> personas = creadorPersonas.getPersonas();
    for (auto persona : personas) {
        parque->agregarPersona(persona);
    }

    pid_t adminPid = administrador.lanzarse();

    lanzarParque(mainPid, parque, adminPid);

    lanzarJuegos(mainPid, juegos);

    lanzarPersonas(mainPid, personas);

    waitHijos(mainPid, juegos, personas);

    Parque::destruir();
    Logger::finalizar();

    return 0;
}

void lanzarParque (const pid_t mainPid, Parque* parque, pid_t adminPid) {
    if (mainPid == getpid()) { // Equivale a adminPid != 0
        parque->setAdministrador(adminPid);
        parque->lanzarse();
    }
}

void waitHijos (const pid_t mainPid, const vector<Juego*>& juegos, const vector<Persona*>& personas) {
    // Espera a que finalicen los Juegos, las Personas, el Parque y el Administrador
    for (int i = 0; i < juegos.size() + personas.size() + 2; i++) {
        if (mainPid == getpid()) {
            int status = 999;
            pid_t pid = wait(&status);
            Logger::log("Termina el proceso " + to_string(pid) + " con status " + to_string(status));
        }
    }
}

void lanzarPersonas (const pid_t mainPid, const vector<Persona*>& personas) {
    for (auto& persona : personas) {
        if (getpid() == mainPid) {
            // creaba persona mala
            persona->lanzarse();
        }
    }
}

void lanzarJuegos (const pid_t mainPid, const vector<Juego*>& juegos) {
    Parque* parque = Parque::getInstance();
    for (auto& juego : juegos) {
        if (mainPid == getpid()) {
            parque->registrarPidJuego(juego->lanzarse());
        }
    }
}

Opciones parsearParametros (int argc, char** argv) {
    Opciones opciones; // Inicializado con valores default

    int opt;
    while ((opt = getopt(argc, argv, "dp:j:l:s:a:")) != -1) {
        switch (opt) {
            case 'd':
                opciones.debug = true;
                break;
            case 'p':
                opciones.personas = string(optarg);
                break;
            case 'j':
                opciones.juegos = string(optarg);
                break;
            case 'l':
                opciones.logName = string(optarg);
                break;
            case 's':
                opciones.sleep = (unsigned int) stoul(optarg);
                break;
            case 'a':
                opciones.alarm = (unsigned int) stoul(optarg);
                break;
            default: /* '?' */
                fprintf(stderr, "Usage: %s [-d] [-p personasFile] [-j juegosFile] [-l logFile] [-s sleepTime] [-a alarmTime] \n", argv[0]);
                exit(EXIT_FAILURE);
        }
    }
    return opciones;
}
