#include "Administrador.h"

using namespace std;

Administrador::Administrador (const MemoriaCompartida<unsigned long>& caja,
                              const Semaforo& semaforoCaja,
                              unsigned int sleepTime) :
        caja(caja), sleepTime(sleepTime), semaforoCaja(semaforoCaja) {
}

Administrador::~Administrador () {

}

void Administrador::comenzar () {
    Logger::log("Comienza el Administrador");
    cout << "Administrador: " << getpid() << endl;

    SignalEventHandler sigInt(SIGINT);
    SignalHandler::getInstance()->registrarHandler(SIGINT, &sigInt);

    while (not sigInt.getSonoAlarma()) {
        semaforoCaja.p();
        unsigned long valorCaja = caja.leer();
        semaforoCaja.v();
        Logger::log("Administrador: La caja actual es " + to_string(valorCaja));
        sleep(sleepTime);
    }

    SignalHandler::getInstance()->removerHandler(SIGINT);
}





