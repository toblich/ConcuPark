#include <cstring>
#include "Semaforo.h"
#include "Logger.h"

using std::string;

Semaforo::Semaforo (const std::string& nombre, const int valorInicial) {
    key_t clave = ftok(nombre.c_str(), 'a');
    this->id = semget(clave, 1, 0666 | IPC_CREAT);

    if (id == -1)
        Logger::log("Error al crear el semaforo: " + string(strerror(errno)));

    this->inicializar(valorInicial);
}

Semaforo::Semaforo (const Semaforo& semaforo) {
    this->id = semaforo.id;
}

Semaforo::~Semaforo () {
}

int Semaforo::inicializar (int valorInicial) const {

    union semnum {
        int val;
        struct semid_ds* buf;
        ushort* array;
    };

    semnum init;
    init.val = valorInicial;
    int resultado = semctl(this->id, 0, SETVAL, init);

    if (resultado == -1)
        Logger::log("Error al inicializar el semaforo: " + string(strerror(errno)));

    return resultado;
}

int Semaforo::p () const {

    struct sembuf operacion;

    operacion.sem_num = 0;    // numero de semaforo
    operacion.sem_op = -1;    // restar 1 al semaforo
    operacion.sem_flg = SEM_UNDO;

    int resultado = semop(this->id, &operacion, 1);

    if (resultado == -1)
        Logger::log("Error al decrementar el semaforo: " + string(strerror(errno)));

    return resultado;
}

int Semaforo::v () const {

    struct sembuf operacion;

    operacion.sem_num = 0;    // numero de semaforo
    operacion.sem_op = 1;    // sumar 1 al semaforo
    operacion.sem_flg = SEM_UNDO;

    int resultado = semop(this->id, &operacion, 1);

    if (resultado == -1)
        Logger::log("Error al incrementar el semaforo: " + string(strerror(errno)));

    return resultado;
}

void Semaforo::eliminar () const {
    Logger::log("Se elimina el semaforo");
    semctl(this->id, 0, IPC_RMID);
}


