#ifndef CONCUPARK_PARQUE_H
#define CONCUPARK_PARQUE_H

#include <map>
#include "Proceso.h"
#include "Persona.h"
#include "MemoriaCompartida.h"
#include "Semaforo.h"

#define CAJA 0
#define ID_PERSONA 1
#define PID_JUEGO 2

typedef struct {
    int tipo;
    int valor;
} MensajeParque;


class Parque : public Proceso {
public:
    virtual ~Parque ();

    static Parque* getInstance () {
        if (instance == NULL) instance = new Parque();
        return instance;
    }

    static void destruir ();

    void agregarPersona (Persona* persona) { personaConID[persona->getId()] = persona; }

    Persona* getPersona (int id) {
        assert(personaConID.find(id) != personaConID.end());
        return personaConID.at(id);
    }

    void setAdministrador (pid_t administrador) { this->pidAdministrador = administrador; }

    const MemoriaCompartida<unsigned long>& getCaja () const { return caja; }

    const Semaforo& getSemaforoCaja () const { return semaforoCaja; }

    void cobrar (int monto);

    void salio (int id);

    void registrarPidJuego (pid_t pid);

private:

    Parque ();

    void comenzar ();

    void salioPersona (int id);

    void procesarMensaje (MensajeParque mensaje);

    std::map<int, Persona*> personaConID;
    std::vector<pid_t> pidJuegos;

    pid_t pidAdministrador;
    MemoriaCompartida<unsigned long> caja;
    Semaforo semaforoCaja;

    static Parque* instance;
};


#endif //CONCUPARK_PARQUE_H
