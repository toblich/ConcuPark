#include "CreadorPersonas.h"

CreadorPersonas::CreadorPersonas (const std::string& filename, std::vector<Juego*>& juegos) : personas() {
    parsearPersonas(filename, juegos);
}

CreadorPersonas::~CreadorPersonas () {
    for (auto& persona : personas) {
        delete persona;
    }
}

void CreadorPersonas::parsearPersonas (const std::string& filename, std::vector<Juego*>& juegos) {
    std::ifstream file(filename);
    int dineroInicial;
    for (int id = 1; file >> dineroInicial; id++) {
        personas.push_back(new Persona(dineroInicial, juegos, id));
    }
    file.close();
}





