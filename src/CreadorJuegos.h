#ifndef CONCUPARK_CREADORJUEGOS_H
#define CONCUPARK_CREADORJUEGOS_H

#include <fstream>
#include "Juego.h"
#include "Opciones.h"

class CreadorJuegos {
public:
    CreadorJuegos (const Opciones& opciones);

    virtual ~CreadorJuegos ();

    const std::vector<Juego*>& getJuegos () const {
        return juegos;
    }

private:
    std::vector<Juego*> juegos;

    void parsearJuegos (const Opciones& opciones);
};


#endif //CONCUPARK_CREADORJUEGOS_H
