#include "Proceso.h"
#include "SignalHandler/SignalHandler.h"


Proceso::Proceso () : pipe(), sigPipe(SIGPIPE) {
    this->pid = -1;
    SignalHandler::getInstance()->registrarHandler(SIGPIPE, &sigPipe);
}

Proceso::Proceso (const Proceso& p) : pipe(p.pipe), sigPipe(p.sigPipe), pid(p.pid) {

}

Proceso::~Proceso () {
    SignalHandler::getInstance()->removerHandler(SIGPIPE);
    SignalHandler::destruir();
}

pid_t Proceso::lanzarse () {
    this->pid = fork();

    if (pid == 0) {
        pipe.setModo(LECTURA);
        this->comenzar();
    } else {
        pipe.setModo(ESCRITURA);
    }

    return pid;
}



