#ifndef CONCUPARK_PROCESO_H
#define CONCUPARK_PROCESO_H

#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include "Pipe.h"
#include "SignalHandler/SignalEventHandler.h"


class Proceso {

public:
    Proceso ();

    Proceso (const Proceso& p);

    virtual ~Proceso ();

    /* Lanza el nuevo proceso y devuelve el pid s*/
    pid_t lanzarse ();

protected:

    /* Comienza la ejecucion del hijo */
    virtual void comenzar () = 0;

    Pipe pipe;
    SignalEventHandler sigPipe;
    pid_t pid;
};


#endif //CONCUPARK_PROCESO_H
