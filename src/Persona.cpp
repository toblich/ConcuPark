#include <assert.h>
#include "Persona.h"
#include "Parque.h"

using std::vector;
using std::pair;
using std::to_string;
using std::cout;
using std::endl;

Persona::Persona (int dineroInicial, vector<Juego*>& juegos, int id) : juegosExistentes(juegos) {
    this->dinero = dineroInicial;
    this->id = id;
}

Persona::Persona (const Persona& p) : dinero(p.dinero), juegosExistentes(p.juegosExistentes), id(p.id) {

}


Persona::~Persona () { }

void Persona::comenzar () {
    Logger::log("Soy la persona con PID " + std::to_string(getpid()));
    std::cout << "Persona: " << getpid() << std::endl;

    const int min = 0;
    const long int max = juegosExistentes.size() - 1;
    srand((unsigned int) getpid());

    const int costoMinimo = getCostoMinimo();

    while (dinero >= costoMinimo) {
        unsigned long indexJuego = (unsigned long) min + (rand() % (max - min + 1));
        assert(indexJuego >= 0);
        assert(indexJuego < juegosExistentes.size());
        int costoJuego = juegosExistentes.at(indexJuego)->getCosto();
        if (costoJuego > dinero) continue;

        Logger::log("Persona " + to_string(id) + ": Me pongo en la fila del juego " + to_string(indexJuego));
        juegosExistentes.at(indexJuego)->agregarPersonaAFila(*this);
        jugar();
        Logger::log("Persona " + to_string(id) + ": Me bajo del juego " + to_string(indexJuego));
    }

    Parque::getInstance()->salio(id);
    Logger::log("SALI DEL PARQUE");
}

void Persona::pagar (int costo) {
    this->dinero -= costo;
    Logger::log("Dinero Restante: " + to_string(dinero));
}

void Persona::jugar () {
    int accionSubir = -1;
    pipe.leer(&accionSubir, sizeof(accionSubir));
    if (accionSubir != SUBIR) {
        Logger::log("Error: Recibi accion: " + std::to_string(accionSubir) + " y soy: " + std::to_string(getpid()));
        exit(-1);
    }
    Logger::log("Me subi al juego");

    int accionBajar = -1;
    pipe.leer(&accionBajar, sizeof(accionBajar));
    if (accionBajar != BAJAR) {
        Logger::log("Error: No me quiero bajar del juego y soy: " + std::to_string(getpid()));
        exit(-1);
    }
    Logger::log("Me baje del juego");
}

void Persona::bajarse () {
    int bajarse = BAJAR;
    pipe.escribir(&bajarse, sizeof(bajarse));
}

void Persona::subirse () {
    int subirse = SUBIR;
    pipe.escribir(&subirse, sizeof(subirse));
}

const int Persona::getCostoMinimo () {
    if (juegosExistentes.size() < 1) {
        Logger::log("ERROR: No hay juegos");
        return 0;
    }
    int minimo = juegosExistentes.at(0)->getCosto();
    for (auto& juego : juegosExistentes) {
        int costo = juego->getCosto();
        if (costo < minimo)
            minimo = costo;
    }

    return minimo;
}













