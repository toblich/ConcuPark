#include <cstring>
#include "Pipe.h"

using std::cout;
using std::to_string;

Pipe::Pipe () {
    int r = pipe(pipefd);
    if (r == -1) {
        Logger::log("Error al crear el pipe");
        exit(-1);
    }
    modo = INDEFINIDO;
}

Pipe::Pipe (const Pipe& pipe) : modo(pipe.modo) {
    this->pipefd[READ] = pipe.pipefd[READ];
    this->pipefd[WRITE] = pipe.pipefd[WRITE];
}

Pipe::~Pipe () {
    switch (modo) {
        case LECTURA: close(pipefd[READ]);
            break;
        case ESCRITURA: close(pipefd[WRITE]);
            break;
        default:
            close(pipefd[READ]);
            close(pipefd[WRITE]);
    }
}

void Pipe::setModo (MODO_PIPE nuevoModo) {
    if (this->modo != INDEFINIDO or nuevoModo == INDEFINIDO) {
        Logger::log("Error: cambiando modo de pipe");
        exit(-1);
    }

    this->modo = nuevoModo;

    if (this->modo == ESCRITURA) {
        close(pipefd[READ]);
    } else {
        close(pipefd[WRITE]);
    }
}

void Pipe::escribir (const void* mensaje, size_t tamanio) {
    if (modo == LECTURA) {
        Logger::log("Error: Se quiere escribir en un pipe que esta en modo lectura");
        exit(-1);
    } else if (modo == INDEFINIDO) {
        setModo(ESCRITURA);
    }

    ssize_t escrito = write(pipefd[WRITE], mensaje, tamanio);

    if (escrito == -1 and errno != EPIPE) {
        Logger::log("Error al escribir en el pipe");
        exit(-1);
    }
}

void Pipe::leer (void* mensaje, size_t tamanio) {
    if (modo == ESCRITURA) {
        Logger::log("Error: Se quiere leer de un pipe que esta en modo escritura");
        exit(-1);
    } else if (modo == INDEFINIDO) {
        Logger::log("Leo de pipe en modo indefinido");
        setModo(LECTURA);
    }

    ssize_t leido = read(pipefd[READ], mensaje, tamanio);

    if (leido == -1 and errno != EINTR and errno != EPIPE) {
        Logger::log("Error al leer del pipe");
        Logger::log(strerror(errno));
        exit(-2);
    }
}











