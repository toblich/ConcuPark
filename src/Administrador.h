#ifndef CONCUPARK_ADMINISTRADOR_H
#define CONCUPARK_ADMINISTRADOR_H

#include "Proceso.h"
#include "MemoriaCompartida.h"
#include "SignalHandler/SignalHandler.h"
#include "Semaforo.h"

class Administrador : public Proceso {
public:
    Administrador (const MemoriaCompartida<unsigned long>& caja, const Semaforo& semaforoCaja, unsigned int sleepTime);

    ~Administrador ();

private:
    void comenzar ();

    MemoriaCompartida<unsigned long> caja;
    Semaforo semaforoCaja;
    unsigned int sleepTime;
};


#endif //CONCUPARK_ADMINISTRADOR_H
