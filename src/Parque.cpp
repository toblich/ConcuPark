#include "Parque.h"

using std::string;
using std::to_string;

Parque* Parque::instance = NULL;

Parque::Parque () : pidJuegos(), personaConID(), caja(string(__FILE__), 'a'), semaforoCaja(string(__FILE__), 1) {
    this->caja.escribir(0);
    this->pidAdministrador = -1;
}

Parque::~Parque () {
}

void Parque::comenzar () {
    assert(pidAdministrador != -1);
    Logger::log("Comienza el Parque");
    std::cout << "Parque: " << getpid() << std::endl;
    while (personaConID.size() > 0) {
        MensajeParque mensaje;
        pipe.leer(&mensaje, sizeof(mensaje));
        procesarMensaje(mensaje);
    }
}

void Parque::salioPersona (int id) {
    Logger::log("Parque: Salio la persona " + to_string(id));
    personaConID.erase(id);
    if (personaConID.size() == 0) {
        Logger::log("Parque vacio");
        for (auto pid : pidJuegos) {
            Logger::log("Parque: Manda senial SIGINT a juego con pid: " + to_string(pid));
            kill(pid, SIGINT);
        }
        kill(pidAdministrador, SIGINT);
        semaforoCaja.eliminar();
    }
}

void Parque::destruir () {
    if (instance != NULL) {
        Logger::log("Se destruye el parque");
        delete instance;
        instance = NULL;
    }
}

void Parque::cobrar (int monto) {
    Logger::log("Se envia el mensaje de cobrar " + to_string(monto) + " al parque");
    MensajeParque mensaje;
    mensaje.tipo = CAJA;
    mensaje.valor = monto;
    pipe.escribir(&mensaje, sizeof(mensaje));
}

void Parque::salio (int id) {
    Logger::log("Se envia el mensaje de que salio la persona " + to_string(id) + " al parque");
    MensajeParque mensaje;
    mensaje.tipo = ID_PERSONA;
    mensaje.valor = id;
    pipe.escribir(&mensaje, sizeof(mensaje));
}

void Parque::registrarPidJuego (pid_t pid) {
    Logger::log("Se envia el mensaje de registrarPidJuego " + to_string(pid) + " al parque");
    MensajeParque mensaje;
    mensaje.tipo = PID_JUEGO;
    mensaje.valor = pid;
    pipe.escribir(&mensaje, sizeof(mensaje));
}

void Parque::procesarMensaje (MensajeParque mensaje) {
    switch (mensaje.tipo) {
        case CAJA: {
            Logger::log("Se suma a la caja el monto: " + to_string(mensaje.valor));
            semaforoCaja.p();
            const unsigned long nuevoMonto = caja.leer() + mensaje.valor;
            caja.escribir(nuevoMonto);
            semaforoCaja.v();
            Logger::log("Parque: Nueva caja: " + to_string(nuevoMonto));
            break;
        } // Nota: No sacar las {} (por scope de nuevoMonto)
        case ID_PERSONA:
            Logger::log("Se da de baja a la persona: " + to_string(mensaje.valor));
            salioPersona(mensaje.valor);
            Logger::log("Cantidad de gente: " + to_string(personaConID.size()));
            break;
        case PID_JUEGO:
            Logger::log("Se da de alta al juego con pid: " + to_string(mensaje.valor));
            pidJuegos.push_back(mensaje.valor);
            Logger::log("Cantidad de juegos: " + to_string(pidJuegos.size()));
            break;
        default:
            Logger::log("ERROR: El parque leyo un tipo de mensaje invalido: " + std::to_string(mensaje.tipo));
            exit(-1);
    }
}





















