#ifndef CONCUPARK_JUEGO_H
#define CONCUPARK_JUEGO_H

#include <vector>
#include "Proceso.h"
#include "SignalHandler/SignalEventHandler.h"
#include "SignalHandler/SignalHandler.h"


class Persona;

#include "Persona.h"


class Juego : public Proceso {
public:
    Juego (int capacidad, int costo, unsigned int sleepTime, unsigned int alarmTime);

    Juego (const Juego& juego);

    virtual ~Juego ();

    int getCosto () const { return costo; }

    void agregarPersonaAFila (Persona& persona);

private:
    void comenzar ();

    void sacarPersonasDeLaFila ();

    void arrancarJuego ();

    void bajarPersonas ();

    SignalEventHandler sigAlarm;
    int capacidad;
    int costo;
    unsigned int sleepTime, alarmTime;
    std::vector<Persona*> genteAdentro;
};


#endif //CONCUPARK_JUEGO_H
